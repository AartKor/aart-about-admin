package aartkorea.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

public class FTPManager {
	FTPClient _FTPClient;
	protected String ftpRoot = "";
	protected String clientOrToRoot = "/";
	protected boolean overwrite = true;
	protected boolean allDirectory = true;
	protected String[] ableNames;
	protected String[] ableExtentions;
	protected String ableFolder;

	protected int localFiledeleted;

	public FTPManager() {
		_FTPClient = new FTPClient();
		System.out.println(_FTPClient.getReplyCode() + ": new FTPClient");
		_FTPClient.setControlEncoding("UTF-8");
		System.out.println(_FTPClient.getReplyCode() + ": Encording");
	}

	public Boolean connectFTP(String host, int port) {
		boolean isSuccess = false;
		try {
			_FTPClient.connect(host, port);
			System.out.println(_FTPClient.getReplyCode() + ": Connect");
			isSuccess = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return isSuccess;
	}

	public Boolean loginFTP(String user, String pwd) {
		boolean isReturn = false;
		try {
			_FTPClient.login(user, pwd);
			System.out.println(_FTPClient.getReplyCode() + ": loginFTP");
			isReturn = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isReturn;
	}

	public Boolean binaryFileType() {
		boolean isReturn = false;
		try {
			_FTPClient.setFileType(FTP.BINARY_FILE_TYPE);
			System.out.println(_FTPClient.getReplyCode() + ": binaryFileType");
			isReturn = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isReturn;
	}

	public Boolean fileTransferMode() {
		boolean isReturn = false;
		try {
			_FTPClient.setFileTransferMode(FTP.STREAM_TRANSFER_MODE);
			System.out.println(_FTPClient.getReplyCode() + ": fileTransferMode");
			isReturn = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isReturn;
	}

	public Boolean makeDir(String dir) {
		boolean isReturn = false;
		try {
			_FTPClient.enterLocalPassiveMode();
			System.out.println(_FTPClient.getReplyCode() + ": enterLocalPassiveMode");
			if (!_FTPClient.changeWorkingDirectory(dir)) {
				System.out.println(_FTPClient.getReplyCode() + ": changeWorkingDirectory");
				_FTPClient.makeDirectory(dir);
				System.out.println(_FTPClient.getReplyCode() + ": makeDir");
				_FTPClient.changeWorkingDirectory(dir);
				System.out.println(_FTPClient.getReplyCode() + ": changeWorkingDirectory");
			}
			_FTPClient.setFileType(FTP.BINARY_FILE_TYPE);
			System.out.println(_FTPClient.getReplyCode() + ": BINARY_FILE_TYPE");

			if (_FTPClient.getReplyCode() == 257) {
				System.out.println("Create Directory");
			}
			if (_FTPClient.getReplyCode() == 500) {
				System.out.println("Aleady Directory");
			}
			isReturn = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isReturn;
	}

	public Boolean fileDownload(String ftpFileName, String localFileName) {
		boolean isReturn = false;
		try {
			File localFile = new File(localFileName);
			if (localFile.exists() && !overwrite)
				return false;
			OutputStream outputStream = new FileOutputStream(localFile);
			_FTPClient.retrieveFile(ftpFileName, outputStream);
			outputStream.close();
			System.out.println(_FTPClient.getReplyCode() + ": fileDownload");
			isReturn = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isReturn;
	}

	public void upload(File clientFile) {
		try {
			String uploadName = getNameByClient(clientFile);
			if (clientFile.isDirectory())
				makeDir(uploadName);
			else {
				FileInputStream inputStream = new FileInputStream(clientFile);

				if (overwrite)
					_FTPClient.storeFile(uploadName, inputStream); // 덮어쓰기
				else
					_FTPClient.appendFile(uploadName, inputStream);
				inputStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Boolean upload(InputStream is, String path, String uploadName) {
		Boolean result = false;
		try {

			makeDir(path);
			System.out.println(_FTPClient.getReplyCode() + ": upload1");

			result = _FTPClient.appendFile(uploadName, is);

			System.out.println(_FTPClient.getReplyCode() + ": Upload2");
			is.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public Boolean delete(String fullName) {
		System.out.println(fullName);
		boolean result = false;
		try {
			result = _FTPClient.deleteFile(fullName);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	protected String getNameByFtpRoot(String name) {
		name = name.substring(ftpRoot.length(), name.length());
		return clientOrToRoot + name;
	}

	protected String getNameByClient(File file) {
		String name = file.getAbsolutePath();
		name = name.substring(clientOrToRoot.length(), name.length());
		return ftpRoot + name;
	}

	public void disconnectFTP() {
		if (_FTPClient == null)
			return;
		try {
			_FTPClient.logout();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (_FTPClient.isConnected()) {
				try {
					_FTPClient.disconnect();

				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		}

	}

}
