package aartkorea.util;

import java.util.Random;

public class StringUtils {
	public static String toUTF8(String str) {
		if (str == null)
			return "";
		try {
			str = new String(str.getBytes("8859_1"), "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}

	public static String content_dispositionParseStr(String strDB) {
		int strDBLength = strDB.length() - "\"".length();
		int lastIdx = strDB.lastIndexOf("filename=") + "filename=\"".length();
		return strDB.substring(lastIdx, strDBLength);
	}

	public static String getExtension(String fileStr) {
		return fileStr.substring(fileStr.lastIndexOf("."), fileStr.length());
	}

	public static String RandomNumber(int length) {
		Random rnd = new Random();
		String numTemp[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		String auth = "";

		for (int i = 0; i < length; i++) {
			auth = auth + numTemp[rnd.nextInt(10)];
		}

		return auth;
	}

}
