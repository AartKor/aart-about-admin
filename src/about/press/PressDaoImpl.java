package about.press;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Part;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import aartkorea.util.FTPManager;
import aartkorea.util.StringUtils;

@Repository
public class PressDaoImpl implements PressDao {

	@Autowired
	SessionFactory sessionFactory;

	private Criteria getCriteria() {
		return getSession().createCriteria(Press.class);
	}

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void add(Press press, Part part) {
		String fileName = "";
		String fileType = "";
		SimpleDateFormat formatter = new SimpleDateFormat("yyMMddHHmmss");
		fileName = formatter.format(new Date()) + StringUtils.RandomNumber(5);

		System.out.println(part.getName());
		System.out.println(part.getHeader("content-disposition"));
		System.out.println(part.getContentType());
		System.out.println(part.getSize());
		System.out.println("" + part.getHeaderNames());

		fileType = StringUtils.getExtension(StringUtils.content_dispositionParseStr(part.getHeader("content-disposition")));

		if (filrUploadFTP(part, fileName + fileType)) {
			press.setImg(fileName + fileType);
		}

		System.out.println("Press id : " + press.getIdPress());
		System.out.println("Press img : " + press.getImg());
		System.out.println("Press title : " + press.getTitle());
		System.out.println("Press contents : " + press.getContents());
		System.out.println("Press source : " + press.getSource());
		System.out.println("Press archives : " + press.getArchives());

		getSession().save(press);

	}

	@Override
	public void update(Press press) {
		getSession().update(press);
	}

	@Override
	public void update(Press press, Part part) {
		String fileName = "";
		String fileType = "";
		SimpleDateFormat formatter = new SimpleDateFormat("yyMMddHHmmss");
		fileName = formatter.format(new Date()) + StringUtils.RandomNumber(5);

		System.out.println(part.getName());
		System.out.println(part.getHeader("content-disposition"));
		System.out.println(part.getContentType());
		System.out.println(part.getSize());
		System.out.println("" + part.getHeaderNames());

		fileType = StringUtils.getExtension(StringUtils.content_dispositionParseStr(part.getHeader("content-disposition")));

		if (filrUploadFTP(part, fileName + fileType)) {
			press.setImg(fileName + fileType);
		}

		System.out.println("Press id : " + press.getIdPress());
		System.out.println("Press img : " + press.getImg());
		System.out.println("Press title : " + press.getTitle());
		System.out.println("Press contents : " + press.getContents());
		System.out.println("Press source : " + press.getSource());
		System.out.println("Press archives : " + press.getArchives());

		getSession().update(press);
	}

	@Override
	public Press get(int id) {
		return (Press) getCriteria().add(Restrictions.eq("idPress", id)).uniqueResult();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Press> list() {
		return getCriteria().list();
	}

	@Override
	public void delete(int id) {
		Press press = (Press) getCriteria().add(Restrictions.eq("idPress", id)).uniqueResult();
		fileDeleteFTP(press.getImg());
		System.out.println(press.getImg());
		getSession().createQuery("delete from Press where idPress = ?").setInteger(0, id).executeUpdate();
	}

	@Override
	public void fileDelete(int id) {
		Press press = (Press) getCriteria().add(Restrictions.eq("idPress", id)).uniqueResult();
		fileDeleteFTP(press.getImg());
		System.out.println(press.getImg());
		getSession().createQuery("UPDATE  Press SET img=? where idPress = ?").setString(0, "").setInteger(1, id).executeUpdate();
	}

	private Boolean filrUploadFTP(Part part, String fullFileName) {

		try {
			InputStream inputStream = null;
			FTPManager _FtpManager = new FTPManager();
			inputStream = part.getInputStream();
			System.out.println("Stream = " + inputStream);

			_FtpManager.connectFTP("upload02k.xdn.kinxcdn.com", 20021);
			_FtpManager.loginFTP("Aart", "Adkxm(!)");
			_FtpManager.upload(inputStream, "about/press", fullFileName);
			_FtpManager.disconnectFTP();

			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	private void fileDeleteFTP(String img) {
		FTPManager _FtpManager = new FTPManager();
		_FtpManager.connectFTP("upload02k.xdn.kinxcdn.com", 20021);
		_FtpManager.loginFTP("Aart", "Adkxm(!)");
		_FtpManager.delete("about/press/" + img);
		_FtpManager.disconnectFTP();
	}
}
