package about.press;

import java.util.Date;
import java.util.List;

import javax.servlet.http.Part;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PressServiceImpl implements PressService {

	@Autowired
	PressDao _PressDao;

	@Override
	public void add(Press press, Part part) {
		press.setRegDate(new Date());
		_PressDao.add(press, part);
	}

	@Override
	public void update(Press press) {
		_PressDao.update(press);
	}

	@Override
	public void update(Press press, Part part) {
		_PressDao.update(press, part);
	}

	@Override
	public Press get(int id) {
		return _PressDao.get(id);
	}

	@Override
	public List<Press> list() {
		return _PressDao.list();
	}

	@Override
	public void delete(int id) {
		_PressDao.delete(id);
	}

	@Override
	public void fileDelete(int id) {
		_PressDao.fileDelete(id);
	}
}
