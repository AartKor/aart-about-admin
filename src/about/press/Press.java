package about.press;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Press {
	/*
	 * Test
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int idPress;

	String img;

	@NotNull
	@Size(min = 1, message = "제목을 입력하세요.")
	String title;

	@NotNull
	@Size(min = 1, message = "내용을 입력하세요.")
	String contents;

	@NotNull
	@Size(min = 1, message = "출처를 입력하세요.")
	String source;

	@NotNull
	@Size(min = 1, message = "아카이브를 입력하세요.")
	String archives;

	@Temporal(TemporalType.TIMESTAMP)
	Date regDate;

	public int getIdPress() {
		return idPress;
	}

	public void setIdPress(int idPress) {
		this.idPress = idPress;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getArchives() {
		return archives;
	}

	public void setArchives(String archives) {
		this.archives = archives;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

}
