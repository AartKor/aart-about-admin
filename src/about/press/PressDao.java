package about.press;

import java.util.List;

import javax.servlet.http.Part;

public interface PressDao {
	void add(Press press,Part part);
	void update(Press press);
	void update(Press press,Part part);
	Press get(int id);
	List<Press> list();
	void delete(int id);
	void fileDelete(int id);

}
