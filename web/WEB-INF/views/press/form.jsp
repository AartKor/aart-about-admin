<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8" />

<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
        Remove this if you use the .htaccess -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>form</title>
<meta name="description" content="" />
<meta name="author" content="Chonglye Chang" />



<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="apple-touch-icon" href="/apple-touch-icon.png" />
<link rel="stylesheet" href="<c:url value="/css/default.css" />" />

<script src="<c:url value="/ckeditor/ckeditor.js" />"></script>
<script>
	
</script>
</head>

<body>
	<div>
		<header>
			<h1>Aart About Admin Form</h1>
		</header>

		<div>
			<section>

				<fieldset>
					<legend> Press 등록 </legend>
					<div>
						<a href="<c:url value='/press/list'/>">목록</a>
					</div>
					<form:form commandName="press" action="/Aart-about-admin/press"
						method="POST" enctype="multipart/form-data">

						<table>
							<tr>
								<th scope="row"><label for="file">이미지</label></th>
								<td><input type="file" name="file" />
								<form:errors path="img" cssClass="smdis-error-message" /></td>
								<%-- <td><form:input path="img" size="100%" /> <form:errors
										path="img" cssClass="smdis-error-message" /></td> --%>
							</tr>
							<tr>
								<th scope="row"><label for="title">제목</label></th>
								<td><form:input path="title" size="100%" /> <form:errors
										path="title" cssClass="smdis-error-message" /></td>
							</tr>
							<tr>
								<th scope="row"><label for="source">출처</label></th>
								<td><form:input path="source" size="100%" /> <form:errors
										path="source" cssClass="smdis-error-message" /></td>
							</tr>
							<tr>
								<th scope="row"><label for="archives">아카이브</label></th>
								<td><form:select path="archives">
										<form:option value="2009" label="2009" />
										<form:option value="2010" label="2010" />
										<form:option value="2011" label="2011" />
										<form:option value="2012" label="2012" />
										<form:option value="2013" label="2013" />
									</form:select></td>
							</tr>
							<tr>
								<th scope="row"><label for="contents">내용</label></th>
								<td><form:errors path="contents"
										cssClass="smdis-error-message" /> <form:textarea
										path="contents" cssClass="ckeditor"></form:textarea></td>
							</tr>

						</table>
						<input type="submit" value="저장" />
					</form:form>
				</fieldset>

			</section>
		</div>

		<footer>
			<p>&copy; Copyright by Aart</p>
		</footer>
		<script>
			function emulAcceptCharset(form) {
				if (form.canHaveHTML) {
					form.acceptCharset;
				}
				return true;
			}
		</script>
	</div>
</body>
</html>
