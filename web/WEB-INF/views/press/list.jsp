<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8" />

<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
        Remove this if you use the .htaccess -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>list</title>
<meta name="description" content="" />
<meta name="author" content="Chonglye Chang" />

<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
<link rel="stylesheet" href="<c:url value="/css/default.css" />" />
</head>

<body>

	<div>
		<header>
			<h1>Aart About Admin List</h1>
		</header>

		<div>
			<section>
				<fieldset>
					<legend>Press 목록</legend>
					<div>
						<a href="<c:url value='/press/form'/>">새로등록</a>
					</div>
					<ul>
						<c:forEach items="${press}" var="press">
							<li>Title:${press.title} ,${press.archives},
								${press.regDate} | <a
								href="<c:url value='/press/${press.idPress}'/>">상세보기</a> | <a
								href="<c:url value='/press/update/${press.idPress}'/>">수정</a>
								| <a href="<c:url value='/press/delete/${press.idPress}'/>">삭제</a>
							</li>
						</c:forEach>
					</ul>

				</fieldset>
			</section>
		</div>

		<footer>
			<p>&copy; Copyright by Chonglye Chang</p>
		</footer>
	</div>

</body>
</html>