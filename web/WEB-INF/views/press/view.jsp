<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8" />

<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
        Remove this if you use the .htaccess -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>view</title>
<meta name="description" content="" />
<meta name="author" content="Chonglye Chang" />

<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="apple-touch-icon" href="/apple-touch-icon.png" />
<link rel="stylesheet" href="<c:url value="/css/default.css" />" />
</head>

<body>
	<div>
		<header>
			<h1>Aart About Admin View</h1>
		</header>

		<div>
			<section>

				<fieldset>
					<legend> Press 상세 </legend>
					<div>
						 <a href="<c:url value='/press/list'/>">목록</a> | <a href="<c:url value='/press/update/${press.idPress}'/>">수정</a> | <a href="<c:url value='/press/delete/${press.idPress}'/>">삭제</a>
					</div>
					<table>
						<tr>
							<th scope="row"><label for="id_press">아이디</label></th>
							<td>${press.idPress}</td>
						</tr>
						<tr>
							<th scope="row"><label for="img">이미지</label></th>
							<td><img src="http://image.aartkorea.com/about/press/${press.img}"/></td>
						</tr>
						<tr>
							<th scope="row"><label for="title">제목</label></th>
							<td>${press.title}</td>
						</tr>
						<tr>
							<th scope="row"><label for="source">출처</label></th>
							<td>${press.source}</td>
						</tr>
						<tr>
							<th scope="row"><label for="archives">아카이브</label></th>
							<td>${press.archives}</td>
						</tr>
						<tr>
							<th scope="row"><label for="contents">내용</label></th>
							<td>${press.contents}</td>
						</tr>
						<tr>
							<th scope="row"><label for="reg_date">날짜</label></th>
							<td>${press.regDate}</td>
						</tr>

					</table>

				</fieldset>

			</section>
		</div>

		<footer>
			<p>&copy; Copyright by Chonglye Chang</p>
		</footer>
	</div>
</body>
</html>
